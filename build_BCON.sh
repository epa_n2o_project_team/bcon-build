#!/usr/bin/env bash

### Build project=BCON: i.e., produce an executable that can process data into a form suitable for a CMAQ/CCTM run.
### For details see https://bitbucket.org/tlroche/bcon-build/raw/HEAD/README.md
### Not in the same `bldmake`-based style as CCTM-build: this is a copy/mod of BCON-build.
### TODO: BCON-build/build_BCON.sh, ICON-build/build_ICON.sh:
### * refactor
### * rebase on bldit/bldmake

### Expects to be run on a linux (tested with RHEL5). Requires
### * run-common code in BCON_utilities.sh
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * `basename`
### * `date`
### * `dirname`
### * `find`
### * `head`
### * `ls`
### * `make`: note this project calls `make` directly, not `bldmake`
### * `pwd`
### * `readlink`
### * `tail`
### * `tee`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
## message-related
THIS="$0"
# for commandline testing, give path
# THIS='/project/inf35w/roche/CMAQ-5.0.1/git/new/BCON-build/build_BCON.sh'
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

## logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
LOG_FP="${THIS_DIR}/${LOG_FN}"

### more constants in function=setup* below 

### common code for build and run
UTILS_FP="${THIS_DIR}/BCON_utilities.sh"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# common code for build and run
if [[ -r "${UTILS_FP}" ]] ; then
  echo -e "${MESSAGE_PREFIX} about to 'source' utilities file='${UTILS_FP}'" 2>&1 | tee -a "${LOG_FP}"
  source "${UTILS_FP}"
else
  echo -e "${ERROR_PREFIX} cannot read utilities file='${UTILS_FP}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
  exit 2
fi

function setup_BCON_build {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    'setup_BCON_common_vars' \
    'setup_BCON_build_vars' \
    'document_BCON_build_vars' \
    'setup_BCON_build_space' \
    'setup_BCON_sources' \
    'setup_MECHS_sources' \
    'setup_BCON_Makefile' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
# this fails: vars don't get set
#    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
# this works: comment it out for NOPing, e.g., to `source`
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 7
    fi
  done
} # function setup_BCON_build

function setup_BCON_build_vars {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### don't need `git` for this build?
  # M3HOME_BACKUP="${M3HOME}.bak"
  # # bitbucket forces lower-case, and infinity does not have bash >= 4
  # PROJECT_NAME="$(echo ${SUPER_NAME} | tr '[:upper:]' '[:lower:]')"
  # REMOTE_REPO="bitbucket.org/tlroche/${PROJECT_NAME}.git"
  # REMOTE_SSH_URI="ssh://git@${REMOTE_REPO}"
  # # gotta use HTTP on EPA systems: can't SSH out. Worse ...
  # # REMOTE_HTTP_URI="https://tlroche@${REMOTE_REPO}" # don't need user for cloning
  # REMOTE_HTTP_URI="https://${REMOTE_REPO}"
  # ... EPA systems don't have SSL certificates, so lotsa password typing :-(
  # GIT_REMOTE_PREFIX='env GIT_SSL_NO_VERIFY=true'
  # off EPA this should not be a problem
  # GIT_REMOTE_PREFIX=''

  ### build prerequisites:

  ## could not get a `bldmake`-based build to work :-(
  # # bldmake
  # export BLDMAKE_DIR="${M3HOME}/BLDMAKE"
  # # export BLDMAKE_BUILDER="${BLDMAKE_DIR}/bldit.bldmake"
  # export BLDMAKE_EXEC="${BLDMAKE_DIR}/bldmake"
  # instead, copy working Makefile
  MAKEFILE_FP="${THIS_DIR}/Makefile"

  ## repo=BCON
  BCON_DIR="${M3MODEL}"
  export REPOROOT="${BCON_DIR}" # Makefile wants this name

  ## repo=MECHS
  MECHS_DIR="${M3HOME}/MECHS"
  export MECH_INC="${MECHS_DIR}/${Mechanism}" # Makefile wants this name

} # function setup_BCON_build_vars

### document current configuration
function document_BCON_build_vars {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  echo -e "${MESSAGE_PREFIX} LOG_FP='${LOG_FP}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} APPL='${APPL}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} BCON_DIR='${BCON_DIR}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} BCON_EXEC_FP='${BCON_EXEC_FP}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} BCON_BUILD_DIR_NAME='${BCON_BUILD_DIR_NAME}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} BCON_BUILD_DIR_PATH='${BCON_BUILD_DIR_PATH}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} CC='${CC}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} CMAQ_BUILD_DIR='${CMAQ_BUILD_DIR}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} COMPILER='${COMPILER}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} CONFIG_CMAQ_PATH='${CONFIG_CMAQ_PATH}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} CPP='${CPP}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} CPP_FLAGS='${CPP_FLAGS}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} CSH_EXEC='${CSH_EXEC}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} C_FLAGS='${C_FLAGS}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} f_FLAGS='${f_FLAGS}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} f90_FLAGS='${f90_FLAGS}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} F_FLAGS='${F_FLAGS}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} F90_FLAGS='${F90_FLAGS}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} FC='${FC}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} FP='${FP}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} INFINITY='${INFINITY}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} IOAPI='${IOAPI}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} IOAPIMOD='${IOAPIMOD}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} IOAPI_DIR='${IOAPI_DIR}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} IOAPI_VERSION='${IOAPI_VERSION}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} LINKER='${LINKER}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} LINK_FLAGS='${LINK_FLAGS}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} M3HOME='${M3HOME}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} M3LIB='${M3LIB}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} M3MODEL='${M3MODEL}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} MAKEFILE_FP='${MAKEFILE_FP}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} MECHS_DIR='${MECHS_DIR}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} MECH_INC='${MECH_INC}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} MODEL='${MODEL}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} Mechanism='${Mechanism}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} ModCommon='${ModCommon}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} ModMech='${ModMech}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} ModType='${ModType}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NETCDF='${NETCDF}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} NETCDF_DIR='${NETCDF_DIR}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} REPOROOT='${REPOROOT}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} SuperMechanism='${SuperMechanism}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} Tracer='${Tracer}'" 2>&1 | tee -a "${LOG_FP}"
  echo -e "${MESSAGE_PREFIX} UBER_CONFIG_CMAQ_PATH='${UBER_CONFIG_CMAQ_PATH}'" 2>&1 | tee -a "${LOG_FP}"
  echo | tee -a "${LOG_FP}" # newline
} # function document_BCON_build_vars

### create build space
function setup_BCON_build_space {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${BCON_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} BCON_BUILD_DIR_PATH not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  elif [[ -d "${BCON_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} build dir='${BCON_BUILD_DIR_PATH}' exists (move or delete it before running this script), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  else
    for CMD in \
      "mkdir -p ${BCON_BUILD_DIR_PATH}" \
    ; do
      echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
	echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
	exit 7
      fi
    done
  fi
} # function setup_BCON_build_space

### copy sources from repo=BCON to build space
function setup_BCON_sources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  BCON_COMMON_DIR="${BCON_DIR}/${ModCommon}" # FQ path
  # TODO: check dir not empty
  if [[ -r "${BCON_COMMON_DIR}" ]] ; then
    for CMD in \
      "cp -p -r ${BCON_COMMON_DIR}/* ${BCON_BUILD_DIR_PATH}/" \
    ; do
      echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
      # comment this out for NOPing, e.g., to `source`
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
	echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
	exit 7
      fi
    done # for CMD
  else
    echo -e "${ERROR_PREFIX} cannot read BCON/${ModCommon} dir='${BCON_COMMON_DIR}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi # [[ -r "${BCON_COMMON_DIR}" ]]

  BCON_TYPE_DIR="${BCON_DIR}/${ModType}" # FQ path
  # TODO: check dir not empty
  if [[ -r "${BCON_TYPE_DIR}" ]] ; then
    for CMD in \
      "cp -p -r ${BCON_TYPE_DIR}/* ${BCON_BUILD_DIR_PATH}/" \
    ; do
      echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
      # comment this out for NOPing, e.g., to `source`
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
	echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
	exit 7
      fi
    done # for CMD
  else
    echo -e "${ERROR_PREFIX} cannot read BCON/${ModType} dir='${BCON_TYPE_DIR}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi # [[ -r "${BCON_TYPE_DIR}" ]]

  # do this only if ModType==profile ?
  if [[ "${ModType}" == 'profile' ]] ; then
    BCON_EXTRA_DIR="${BCON_DIR}/prof_data/${SuperMechanism}" # FQ path
    if [[ -r "${BCON_EXTRA_DIR}" ]] ; then
      for CMD in \
	"cp -p -r ${BCON_EXTRA_DIR}/* ${BCON_BUILD_DIR_PATH}/" \
      ; do
	echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
	# comment this out for NOPing, e.g., to `source`
	eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
	if [[ $? -ne 0 ]] ; then
	  echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
	  exit 7
	fi
      done # for CMD
    else
      echo -e "${ERROR_PREFIX} cannot read extra BCON dir='${BCON_EXTRA_DIR}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
      exit 2
    fi # [[ -r "${BCON_EXTRA_DIR}" ]]
  fi # [[ "${ModType}" == 'profile' ]]
} # function setup_BCON_sources

### copy sources from repo=MECHS to build space
function setup_MECHS_sources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  MECHS_MECH_DIR="${MECHS_DIR}/${Mechanism}" # FQ path
  # TODO: check dir not empty
  if [[ -r "${MECHS_MECH_DIR}" ]] ; then
    for CMD in \
      "cp -p -r ${MECHS_MECH_DIR}/* ${BCON_BUILD_DIR_PATH}/" \
    ; do
      echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
      # comment this out for NOPing, e.g., to `source`
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
	echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
	exit 7
      fi
    done # for CMD
  else
    echo -e "${ERROR_PREFIX} cannot read MECHS/${Mechanism} dir='${MECHS_MECH_DIR}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi # [[ -r "${MECHS_MECH_DIR}" ]]

  MECHS_TRACER_DIR="${MECHS_DIR}/${Tracer}" # FQ path
  # TODO: check dir not empty
  if [[ -r "${MECHS_TRACER_DIR}" ]] ; then
    for CMD in \
      "cp -p -r ${MECHS_TRACER_DIR}/* ${BCON_BUILD_DIR_PATH}/" \
    ; do
      echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
      # comment this out for NOPing, e.g., to `source`
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
	echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
	exit 7
      fi
    done # for CMD
  else
    echo -e "${ERROR_PREFIX} cannot read MECHS/${Mechanism} dir='${MECHS_TRACER_DIR}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi # [[ -r "${MECHS_TRACER_DIR}" ]]
} # function setup_MECHS_sources

### copy working Makefile, since I can't make the `bldmake`-based bldit.bcon process work :-(
function setup_BCON_Makefile {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -r "${MAKEFILE_FP}" ]] ; then
    for CMD in \
      "cp -p ${MAKEFILE_FP} ${BCON_BUILD_DIR_PATH}/" \
    ; do
      echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
      # comment this out for NOPing, e.g., to `source`
      eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
      if [[ $? -ne 0 ]] ; then
	echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
	exit 7
      fi
    done # for CMD
  else
    echo -e "${ERROR_PREFIX} cannot read Makefile='${MAKEFILE_FP}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 2
  fi # [[ -r "${MAKEFILE_FP}" ]]
} # function setup_BCON_Makefile

function build_BCON {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   [[ -z "${BCON_BUILD_DIR_PATH}" ]] ; then
    echo -e "${ERROR_PREFIX} BCON_BUILD_DIR_PATH not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 5
  fi

  # TODO: test `make`
  for CMD in \
     "ls -alt ${BCON_BUILD_DIR_PATH}" \
     "pushd ${BCON_BUILD_DIR_PATH}" \
     "make" \
     "popd" \
     "ls -al ${BCON_EXEC_FP}" \
     "ls -alt ${BCON_BUILD_DIR_PATH}" \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${ERROR_PREFIX} ${CMD}: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 7
    fi
  done
} # function build_BCON

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

MESSAGE_PREFIX="${THIS_FN}::main loop: "
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

for CMD in \
  'setup_BCON_build' \
  'build_BCON' \
; do
  echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}" # comment this out for NOPing, e.g., to `source`
# this fails, e.g.: 'build_BCON.sh::build_BCON: ERROR: BCON_BUILD_DIR_PATH not defined'
# though BCON_BUILD_DIR_PATH is both defined and used in `setup_BCON_build`
#    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
# this works: comment it out for NOPing, e.g., to `source`
  eval "${CMD}"
  if [[ $? -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
    exit 9
  fi
done

exit 0 # success!
