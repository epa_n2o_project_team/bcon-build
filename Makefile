# take all following from environment by default (edit as necessary)
# MODEL = BCON_CMAQv5_0_1_Linux2_x86_64intel
# CC         = $(myCC)
# CPP        = $(FC)
# CPP_FLAGS  = 
# C_FLAGS    = -O2 -DFLDMN
# F90_FLAGS  = -free -O3 -fno-alias -mp1                        -I /home/wdx/lib/x86_64i/intel/ioapi_3.1/Linux2_x86_64ifort -I.
# FC = $(myFC)
# F_FLAGS    = -fixed -132 -O3 -override-limits -fno-alias -mp1 -I /home/wdx/lib/x86_64i/intel/ioapi_3.1/Linux2_x86_64ifort -I.
# IOAPI      = -L/home/wdx/lib/x86_64i/intel/ioapi_3.1/Linux2_x86_64ifort -lioapi
# LINKER     = $(FC)
# LINK_FLAGS = -i-static
# MECH_INC   = /project/inf35w/roche/CMAQ-5.0.1/git/new/MECHS/cb05tucl_ae6_aq
# NETCDF     = -L/home/wdx/lib/x86_64i/intel/netcdf/lib -lnetcdf
# REPOROOT   = /project/inf35w/roche/CMAQ-5.0.1/git/new/BCON

LIBRARIES = $(IOAPI) $(NETCDF)

INCLUDES =  \
  -DSUBST_GRID_ID= \
  -DSUBST_RXCMMN=\"$(MECH_INC)/RXCM.EXT\" \
  -DSUBST_RXDATA=\"$(MECH_INC)/RXDT.EXT\"

GLOBAL_MODULES = \
  CGRID_SPCS.o \
  HGRD_DEFN.o \
  VGRD_DEFN.o

COMMON = \
  bcon.o \
  findex.o \
  gc_spc_map.o \
  get_envlist.o \
  lat_lon.o \
  lr_interp.o \
  lst_spc_map.o \
  ngc_spc_map.o \
  opn_bc_file.o

PROFILE = \
  prof_bcout.o \
  prof_driver.o \
  prof_vinterp.o

OBJS = \
  $(GLOBAL_MODULES) \
  $(COMMON) \
  $(PROFILE)

.SUFFIXES: .F .f .c .F90 .f90

$(MODEL): $(OBJS)
	$(LINKER) $(LINK_FLAGS) $(OBJS) $(LIBRARIES) -o $@

.F.o:
	$(FC) -c $(F_FLAGS) $(CPP_FLAGS) $(INCLUDES) $<

.f.o:
	$(FC) -c $(f_FLAGS) $<

.F90.o:
	$(FC) -c $(F90_FLAGS) $(CPP_FLAGS) $(INCLUDES) $<

.f90.o:
	$(FC) -c $(f90_FLAGS) $<

.c.o:
	$(CC) -c $(C_FLAGS) $<

clean:
	rm -f $(OBJS) $(MODEL) *.mod

# dependencies

CGRID_SPCS.o:	$(MECH_INC)/RXCM.EXT $(MECH_INC)/RXDT.EXT
bcon.o:	HGRD_DEFN.F VGRD_DEFN.F CGRID_SPCS.F
gc_spc_map.o:	CGRID_SPCS.F
lst_spc_map.o:	CGRID_SPCS.F
ngc_spc_map.o:	CGRID_SPCS.F
opn_bc_file.o:	HGRD_DEFN.F VGRD_DEFN.F CGRID_SPCS.F
prof_bcout.o:	HGRD_DEFN.F VGRD_DEFN.F CGRID_SPCS.F
prof_driver.o:	CGRID_SPCS.F
prof_vinterp.o:	HGRD_DEFN.F VGRD_DEFN.F
